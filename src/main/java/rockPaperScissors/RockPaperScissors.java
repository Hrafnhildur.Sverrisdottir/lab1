package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public String winner(String computer, String user) { // lagar ein method som returnerer kven som vinner
        if (computer.equals(user)){
            return "tie";
        }
        if (((computer == "rock") && (user == "paper")) || ((computer == "paper") && (user == "scissors")) || ((computer == "scissors") && (user == "rock")) ){
            return "Human";
        }
        return "Computer";
    }

    public void run() {
        String exit = "n"; //java var ikkje heilt glad i samanlikning med tekst og variabel, derfor ekstra variabel for samanlikning med answer
        String answer = "";
        while (answer != exit) {
               System.out.println("Let's play round " + roundCounter);
               String user = readInput("Your choice (Rock/Paper/Scissors)?"); //Bruker den allerede lagde readInput funksjonen for å få brukarens valg
               int random = (int)(Math.random()*(2 - 0 + 1) + 0); //Finn eit tilfeldig tall mellom 0 og 2 for å hente computer sitt svar fra rpsChoises
               String computer = rpsChoices.get(random); //hentar index random fra rpschoises
               String winner = winner(computer, user); //brukar method lagd tidlegare
               if (winner == "tie"){ // kode som kun køyrer om det blir uavgjort
                   roundCounter += 1;
                   System.out.println("Human chose " + user + ", computer chose " + computer + ". It's a tie!");
                   System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                   answer = readInput("Do you wish to continue playing? (y/n)?"); //sjekkar om user vil fortsette, avsluttar eller startar loop på nytt
                   if (answer.equals(exit)){
                       System.out.println("Bye bye :)");
                       return;
                   }
                   continue;
                }
                if (winner == "Human"){ //dersom human vinner
                    humanScore += 1;
                    roundCounter += 1;
                    System.out.println("Human chose " + user + ", computer chose " + computer + ". " + winner + " wins!");
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                //    System.out.println("Do you wish to continue playing? (y/n)?");
                    answer = readInput("Do you wish to continue playing? (y/n)?");
                    if (answer.equals(exit)){
                       System.out.println("Bye bye :)");
                       return;
                    }
                    continue;
                }
                computerScore += 1; //dersom det ikkje er tie og human ikkje vann veit me at computer vann, da køyrer denne bolken
                System.out.println("Human chose " + user + ", computer chose " + computer + ". " + winner + " wins!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            //    System.out.println("Do you wish to continue playing? (y/n)?");
                answer = readInput("Do you wish to continue playing? (y/n)?");
                   if (answer.equals(exit)){
                       System.out.println("Bye bye :)");
                       return;
                   }
                roundCounter += 1;

               
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
